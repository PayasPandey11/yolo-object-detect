﻿Multiple Object Detection(YOLO v2)

This is an sample app which enables the user to detect multiple object in the image.
The algorithm provides each prediction with its score and bounds the object in a box.
The algorithm always provides to manually input tag for an image if not recognised.

Requirements

Python 3
Django
Keras
Tensorflow
Numpy
h5py (For Keras model serialization.)
Pillow (For rendering test results.)
pydot-ng (Optional for plotting model.)


Steps :

1. Unizip Marsplayapi.tar (which is the application folder)
2. Run the django application normally (python manage.py runserver)
3. Navigate to 127.0.0.1/detect/ and upload any photo to test.
