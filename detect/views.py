#from bson.json_util import dumps
import codecs, json

# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import FileSystemStorage
from yad2k import test_yolo
from django.core import management
import shutil
import argparse
import os

@csrf_exempt
def index(request):
    if request.method == 'POST' and request.FILES['media']:

        myfile = request.FILES["media"]
        error = False
        if validate_file(myfile.name):
            print("name==========",myfile.name)
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            image_info = test_yolo._main()
            print("Image info --> ",image_info)
            uploaded_file_url = "../" + image_info["image_path"]
            src = "yad2k/images/" + filename
            is_predicted = 1
            if image_info["predicted_classes"] == []:
                is_predicted = 0
            new_file = 'yad2k/images/original/' + myfile.name
            if os.path.isfile(new_file):
                print("FileExists")
                os.remove(src)
            else:
                shutil.move(src, "yad2k/images/original/")
            
            original_file_url  = "yad2k/images/original/" + filename
            print(original_file_url)
            return render(request, 'detect/index.html', {
                'image_info':image_info,'uploaded_file_url': uploaded_file_url,'original_file_url':original_file_url,'p_c':is_predicted
            })
        else:
            error = True
            return render(request, 'detect/index.html', { "error":error
            })


    return render(request, 'detect/index.html')


def validate_file(myfile):
    VALID_IMAGE_EXTENSIONS = [
    ".jpg",
    ".jpeg",
    ".png",
    ".gif",]
    return(any([myfile.lower().endswith(e) for e in VALID_IMAGE_EXTENSIONS]))





@csrf_exempt
def api(request):
    if request.method == 'POST' and request.FILES['apimedia']:

        myfile = request.FILES["apimedia"]
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        image_info = test_yolo._main()
        image_info.pop('image', None)
        print("Image info --> ",image_info)


        src = "yad2k/images/" + filename
        is_predicted = 1
        if image_info["predicted_classes"] == []:
            is_predicted = 0
        new_file = 'yad2k/images/original/' + myfile.name
        if os.path.isfile(new_file):
            print("FileExists")
            os.remove(src)
        else:
            shutil.move(src, "yad2k/images/original/")
        json.dumps(image_info)
        return image_info
